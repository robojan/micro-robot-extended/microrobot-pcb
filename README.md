# MicroRobot++ PCB files
This repository contains the PCB files for the MicroRobot++ robot. 
This small robot is a baseboard for the orange Pi CM4 or the raspberry PI CM4 with reduced features.

The design has been made in altium designer.

## Features
- Wireless charging
- USB charging
- 4x DC motor control
- ICM-42688-P 6-axis IMU
- LIS3MDL 3-axis magnetometer
- 4x ICS-43434 I2S microphone array
- Speaker
- 4 RGB LEDs
- IMX708 camera
- Soft power button
- Fuel guage
- USB power delivery
- USB 3.0 type-c
- Displayport over type-c
- Management microcontroller STM32L412
  - RTC
  - LED driver
  - Motor control
  - HAT eeprom emulation
  - IMU clock generation
  - OTA update